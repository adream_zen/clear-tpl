'use strict'

import gulp			from 'gulp';
import config		from './tasks/config';
import browserSync	from 'browser-sync';
import runSequence 	from 'run-sequence';

// gulp tasks

import styles, {finale}		from "./tasks/styles";
import clean				from "./tasks/clean";
import move, {selectify} 	from "./tasks/move";
import scripts				from "./tasks/scripts";
import customizer 			from "./tasks/customizer";
import images				from "./tasks/images";
import watch				from "./tasks/watch";


gulp.task('styles',			styles);
gulp.task('styles:finale',	finale);
gulp.task('move',			move);
gulp.task('move:selected',  selectify);
gulp.task('scripts',		scripts);
gulp.task('customizer',		customizer);
gulp.task('images',			images);
gulp.task('clean',			clean);
gulp.task('watch',			watch);


gulp.task('run', (done) =>{
	return runSequence('build',['watch','move:selected'], done);
});

gulp.task('default', ['clean','styles','scripts','customizer','move','images','watch']);

gulp.task('build',['clean','scripts','customizer','styles','images','move']);

gulp.task('final',['clean','scripts','customizer','styles:finale','images','move']);