'use strict';

import gulp				from 'gulp';
import config			from './config';
import reportErr		from './notify';
import plumber			from 'gulp-plumber';
import browserSync		from 'browser-sync';

import sass				from 'gulp-sass';
import autoprefixer		from 'gulp-autoprefixer';
import moduleImporter 	from 'sass-module-importer';
import sourcemaps		from 'gulp-sourcemaps';
import stripCssComments from 'gulp-strip-css-comments';

export default () => {
	return gulp.src(config.styles.source)
		.pipe(plumber({
			errorHandler: function(error) {
				reportErr(error, this);
			}
		}))
		.pipe( sourcemaps.init() )
		.pipe( sass( {importer: moduleImporter(), outputStyle: 'compact'} ))
		.pipe( autoprefixer({
			browsers: ['last 3 versions'],
			cascade: false
			}) )
		.pipe( stripCssComments() )
		.pipe( sourcemaps.write("/maps") )
		.pipe( gulp.dest(config.styles.destination) )
		.pipe( browserSync.stream() );
}
export let finale = () => {
	return gulp.src(config.styles.source)
		.pipe(plumber({
			errorHandler: function(error) {
				reportErr(error, this);
			}
		}))
		.pipe( sass( {importer: moduleImporter(), outputStyle: 'compressed'} ))
		.pipe( autoprefixer({
			browsers: ['last 3 versions'],
			cascade: false
			}) )
		.pipe( stripCssComments() )
		.pipe( gulp.dest(config.styles.destination) );
}