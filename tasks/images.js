'use strict';

import gulp			from 'gulp';
import config		from './config';
import reportErr	from './notify';
import browserSync	from 'browser-sync';

import image 		from 'gulp-image';

export default () => {
	return gulp.src(config.images.source)
		.pipe(
			image({
				pngquant: true,
				optipng: true,
				zopflipng: false,
				jpegRecompress: false,
				jpegoptim: true,
				mozjpeg: true,
				guetzli: false,
				gifsicle: true,
				svgo: true,
				concurrent: 10
			})
		)
		.pipe(gulp.dest(config.images.destination))
		.on('error', function(error) {
			reportErr(error, this);
		});
}