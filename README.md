# Clear php pages template 

### Gulp tasks
- Clean - Clear dest directory.
- [Images](https://github.com/1000ch/gulp-image) - Compress images and save in dest direcory.
- Scripts - Import, convert, compress scripts and save in dest directory.
- Styles - Import and compress sass styles and save in dest directory.
- Move - Move other files to dest folder
- [Watch](https://github.com/floatdrop/gulp-watch) - Check files changes and reload browser by BrowseSync.

#All template changes do in template directory.

### Suggest to use technologies for style
- [BEM](https://nafrontendzie.pl/metodyki-css-2-bem/)
- [SASS/SCSS](http://www.merixstudio.pl/blog/co-daje-sass-dlaczego-warto-uzywac-sass/)

### #1 Geting start

By comand line get in to <xampp/htdocs> (or your folder) and make dir
```bash
	mkdir <project name> && cd <project name>
```

Clone repository
```bash
	git clone git@bitbucket.org:adream_zen/clear-tpl.git _project
```
Get into _project folder
```bash
	cd _project
```

Install node.js with modules
```bash
	npm install
```

After instalation run gulp build to creat theme from project files
```bash
	gulp build
```

Change git repository 
```bash
	git config remote.origin.url git@bitbucket.org:Adream_zen/nowe.git
```

Check git config
```bash
	git config --list
```

For run gulp witch watch
```bash
	gulp run
```

For create server fianl file version, use:
```bash
	gulp final
```


* * *

###### Include
- [node.js](https://nodejs.org/en/)
- [gulp.js](http://gulpjs.com) + tasks
- [ECMAScript 6](https://babeljs.io/learn-es2015/) [PL](http://mateuszroth.pl/komp/article-javascriptes6.php)
- Babel
- [SCSS](http://sass-lang.com)
	- [Normalize.scss](http://nicolasgallagher.com/about-normalize-css/)
	- [Bootstrap v4 - alpha](https://github.com/twbs/bootstrap/tree/v4-dev)
	- HTML vs. PSD
	- [Responsive navigation](https://codepen.io/Krzywy14/pen/WpMwXN)
- Plugins
	- tab swither
	- scroll to top
	- achor scroll
	- parallax
	- user scroll stop
	- cokies plugin
	- wordSizer
	- [counters](https://codepen.io/shivasurya/pen/FatiB)
	- Adoordion
	- Gallery based on img alt
	- Preloader
	- [Curtain](https://codepen.io/rontav/pen/rVbXqP)
- IE 10 if
- PHP fucntion:
	- Random string length genrator(Lorem Ipsum)
	- custom content size
	- img include 
	- background image
 


* * *

#Plugin exemples used

## Tabs
###### html
```bash
	<ul class="tabs__nav">
		<li>11111</li>
		<li>22222</li>
		<li>33333</li>
	</ul>
	<ul class="tabs__wrap">
		<li>111111</li>
		<li>222222</li>
		<li>333333</li>
	</ul>
```
###### Script
```bash
	
	import tabs from './tabs';

	$(()=>{
		new tabs(".tabs__nav", ".tabs__wrap");
	});

```
###### SCSS
```bash

	@import "components/tabs";

```

## Scroll to top

###### HTML - after <body>
```bash
	<div id="go_to_top"></div>
```
###### SCSS
```bash
	@import "components/scrollTop";
```
###### JavaScript
```bash
	import scrollTop 	from './scrollTop';
	
	$(()=>{
		new scrollTop();
	});
```

## User scroll - stop plugin scroll when user scoll
###### JavaScript
```bash
	import userScroll 	from './userScroll';

	$(()=>{
		new userScroll();
	});
```


## AchorNav - scroll to achor when link start with #

###### HTML
```bash
	<a href="#name">Achor name</a>


	<a name="name"></a>
```
###### JavaScript
```bash
	import achorNav 		from './achorNav';

	$(()=>{
		new achorNav();
	});
```


## Parallax
###### HTML
```bash
	<div id="parallax"></div>
```
###### SCSS
```bash
	background-image: url(img.jpg)
	background-position: center 40%;
```
###### JavaScript
```bash
	import parallax 		from './parallax';

	$(()=>{
		new parallax("#parallax");
	});
```



## Cookies
###### HTML
```bash
	<?php // include('view/parts/cookies.php'); ?>
	uncomment this line
```
###### SCSS
```bash
	@import "components/cokies";
	
	change in this file:

	.accept-btn {
		/*  Stylujemy pod projekt  */
		background: $base;
		padding: 10px 20px;
		color: $base-color;
	}
```
###### JavaScript
```bash
	import cokies 		from './cokies';

	$(()=>{
		new cokies();
	});
```
## Parallax
###### HTML
```bash
	<div class="count">200</div>
```
###### JavaScript
```bash
	import counters 	from './counters';

	$(()=>{
		new tabs(".count");
	});
```

## Acordeon
###### HTML
```bash
	<ul class="acordeon">
		<li title="tytul 1">
			<p>Lorem ipsum dolor sit amet.</p>
		</li>
	</ul>
```
###### SCSS
```bash
	@import "components/acordeon";
```
###### JavaScript
```bash
	import acordeon 	from './acordeon';

	$(()=>{
		new acordeon(".acordeon",);
	});

```

## Galley
###### HTML
```bash
	<img class="gallery__item" src="photo.jpg" alt="photo.jpg">
```
###### SCSS
```bash
	@import "components/gallery";

```
###### JavaScript
```bash
	import gallery			from './gallery';

	$(()=>{
		new gallery(".gallery__item");
	});
```
## Random text generator
###### HTML / PHP 
	Print random lenght string from lorem ipsum 50 words
```bash
	<?php random_text() ?> <!-- print 50 - 399 char -->
	<?php random_text(60) ?>  <!-- print 50 - 60 char -->
```

# PHP fuctions used
## Image include
###### HTML/PHP
```bash
	<?php img('nine.png'); ?>
```

## Background image
```bash
	<div <?php bgimg("image.jpg")) ?> ></div>
```

## Background image from thumbnail
```bash
	<div <?php bgth(get_the_post_thumbnail_url())) ?> ></div>
```