<input type="checkbox" id="m_nav">
<div class="header--wrap">
	<div class="logo fixed">
		<a href="<?php echo $home ?>">
			<?php img("logo.png") ?>
		</a>
	</div>
	<label for="m_nav" id="mobile-hamburger" onclick>
		<span></span>
		<span></span>
		<span></span>
		<span></span>
	</label>
</div>
<nav>
	<ul>
		<li><a href="#">Lorem.</a></li>
		<li><a href="#">Deleniti!</a></li>
		<li><a href="#">Temporibus?</a></li>
		<li><a href="#">Repellat.</a></li>
		<li><a href="#">Odio.</a></li>
		<li><a href="#">Molestiae.</a></li>
	</ul>
</nav>